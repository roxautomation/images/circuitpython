#!/bin/bash
# Build the docker image
set -e
set -x

source config.sh

echo "Building docker image $IMG_NAME"

docker build -t $IMG_NAME:$VERSION \
            --build-arg BUILD_FLAGS="-j$(nproc)" \
            --build-arg VERSION="$VERSION" \
            ./docker


docker tag $IMG_NAME:$VERSION $IMG_NAME:latest
