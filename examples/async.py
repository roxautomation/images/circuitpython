""" simple async example """

import asyncio


async def loop(id: int, delay: float = 0.1):
    for i in range(5):
        print(f"loop {id} : {i}")
        await asyncio.sleep(delay)


async def main():
    coros = [loop(1), loop(2, 0.5), loop(3, 1.0)]
    await asyncio.gather(*coros)


asyncio.run(main())
