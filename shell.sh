#!/bin/bash

source config.sh

# start image from gitlab
docker run -it --rm \
        -v $(pwd)/examples:/workspace \
        -w /workspace \
        $IMG_NAME \
        bash
