# Circuitpython Unix image

Current version: 9.1.1

This is a unix port of circuitpython used for local development and CI runs.


## Run

```
docker run -it roxauto/circuitpython:<version>
```



## Building

1. update `VERSIOIN` in `config.sh`.
2. update/add bundle file in `docker/bundle` (TODO: automatic latest version download)
3. build locally with `./build.sh`
4. enter bash shell with `shell.sh`
5. gitlab CI wil automatically build and push images.

## Library files

location *in container* is `/usr/lib/micropython` . Remove this folder if you don't want to use built-in bundle.


## Changelog

**9.1.1** update circuipython
